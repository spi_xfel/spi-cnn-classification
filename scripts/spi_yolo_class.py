#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Classify diffraction images in CXI format by YOLO.
Author: Alexandr Ignatenko, Sergey Bobkov
"""

import os
import argparse
import io
import shutil
import subprocess
from tempfile import TemporaryDirectory
import h5py as h5
import matplotlib as mpl
import matplotlib.pyplot as plt
import tqdm

mpl.rcParams['image.cmap'] = 'jet'
mpl.rcParams['image.interpolation'] = 'nearest'

class DarknetTester():
    """Class to test images with darknet"""
    input_msg = "Enter Image Path:"
    encoding = "ascii"

    def __init__(self, darknet, spec, cfg, weights, verbose=False):
        self.darknet = darknet
        self.spec = spec
        self.cfg = cfg
        self.weights = weights
        self.verbose = verbose
        self.darknet_proc = None
        self.output_reader = None

    def start(self):
        """Start darknet process"""
        if self.darknet_proc is not None:
            raise ValueError("DarknetTester was already started")

        proc_args = [self.darknet,
                     'detector',
                     'test',
                     self.spec,
                     self.cfg,
                     self.weights]

        self.darknet_proc = subprocess.Popen(proc_args,
                                             stdin=subprocess.PIPE,
                                             stdout=subprocess.PIPE,
                                             stderr=subprocess.DEVNULL)

        self.output_reader = io.TextIOWrapper(self.darknet_proc.stdout,
                                              encoding=self.encoding,
                                              errors='ignore')

        proc_out = self._read_output()
        if self.verbose:
            print("STDOUT:\n", proc_out)

        if self.input_msg not in proc_out:
            raise ValueError("{} do not accept input images".format(self.darknet))

    def stop(self):
        """Stop darknet process"""
        if self.darknet_proc is None:
            raise ValueError("DarknetTester was not started")

        self.darknet_proc.terminate()
        self.darknet_proc = None

    def test_image(self, image_path):
        """Test image with darknet

        Keyword arguments:
        image_path -- path to input image

        Return:
        result -- True if darknet finds some class, False othewise
        """
        if self.darknet_proc is None:
            raise ValueError("DarknetTester was not started")

        if self.darknet_proc.poll() is not None:
            raise ValueError("DarknetTester was terminated")

        self._send_input(image_path)
        proc_out = self._read_output()
        if self.verbose:
            print("STDOUT:\n", proc_out)

        score = 0
        for line in proc_out.split('\n'):
            if line.endswith("%"):
                try:
                    score = int(line.split(' ')[-1][:-1]) / 100.0
                except ValueError:
                    print('Error parsing line: {}'.format(line))
                    continue

        if self.verbose:
            print("Detected score: ", score)

        return score > 0

    def _send_input(self, input_msg):
        if not input_msg.endswith("\n"):
            input_msg += "\n"

        self.darknet_proc.stdin.write(input_msg.encode(encoding=self.encoding))
        self.darknet_proc.stdin.flush()

    def _read_output(self):
        proc_out = ""

        while True:
            proc_out += self.output_reader.read(1)
            if self.input_msg in proc_out:
                break

        return proc_out

    def __del__(self):
        if self.darknet_proc is not None:
            self.stop()


def test_cxi_file(input_file, dn_tester, dset_name, tmpdir, size=None, sel_area=None, keep_images=False):
    """Extract images from CXI file to jpeg

    Keyword arguments:
    input_file -- input CXI file with images
    dn_tester -- object with test_image() function
    dset_name -- name of result selection dataset
    tmpdir -- folder to save temporary jpeg
    size -- width and height of output images in pixels (same value)
    sel_range -- selected range in image data
    """
    if sel_area is not None:
        assert len(sel_area) == (4), "'sel_area' should include 4 values"

    with h5.File(input_file, 'a') as h5file:
        entry = h5file['entry_1']
        image_keys = sorted([k for k in entry.keys() if k.startswith('image_')],
                            key=lambda k: int(k[6:]))

        # for k in image_keys:
        for k in tqdm.tqdm(image_keys, position=0):
            image_group = entry[k]
            data_dset = image_group['data']
            run_num_dset = image_group['run_number']
            index_dset = image_group['index']

            if dset_name in image_group:
                del image_group[dset_name]

            select_dset = image_group.create_dataset(
                dset_name,
                shape=(data_dset.shape[0],),
                fillvalue=0,
                dtype=int)

            for i, pattern in enumerate(tqdm.tqdm(data_dset, position=1)):
            # for i, pattern in enumerate(data_dset):
                run_num = run_num_dset[i]
                index = index_dset[i]

                frame_path = "{}_{}.png".format(run_num, index)
                frame_path = os.path.join(tmpdir, frame_path)

                if sel_area is not None:
                    pattern = pattern[sel_area[0]:sel_area[1],
                                      sel_area[2]:sel_area[3]]

                if i == 0:
                    if size is None:
                        figsize = (pattern.shape[0] / 100,
                                   pattern.shape[1] / 100)
                        aspect = 'equal'
                    else:
                        figsize = (size/100, size/100)
                        aspect = 'auto'

                    fig, axes = plt.subplots(figsize=figsize)
                    im_h = axes.imshow(pattern, aspect=aspect)
                    # fig, axes = plt.subplots(figsize=(12.4,24.49))
                    # im_h = axes.imshow(pattern)
                    axes.axis('off')
                    axes.axes.get_xaxis().set_visible(False)
                    axes.axes.get_yaxis().set_visible(False)
                    axes.set_frame_on(False)
                else:
                    im_h.set_data(pattern)

                fig.savefig(frame_path, bbox_inches='tight', dpi=100, pad_inches=0)

                select_dset[i] = dn_tester.test_image(frame_path)

                if not keep_images:
                    os.remove(frame_path)

            plt.close(fig)


def copy_file_for_write(input_file, out_file):
    """Create copy of file with write access

    Keyword arguments:
    input_file -- input file
    out_file -- path to output file
    """
    out_dir = os.path.dirname(out_file)
    if out_dir:
        os.makedirs(out_dir, exist_ok=True)

    shutil.copy(input_file, out_file)
    subprocess.call(["/bin/chmod", "u+w", out_file])


def parse_area(area_str):
    """Parse area input into array of 4 values"""
    if isinstance(area_str, str) and len(area_str.split(',')) == 4:
        return [int(val) for val in area_str.split(',')]
    else:
        return None


def main():
    parser = argparse.ArgumentParser(
        description="Classify diffraction images in CXI format by YOLO")
    parser.add_argument('cxi_file', metavar='FILE', help='Input CXI file')
    parser.add_argument('-o', '--out_file', help="Output file")
    parser.add_argument('-t', '--tmp', help="Temporary directory")
    parser.add_argument('-a', '--area', metavar='Y1,Y2,X1,X2', type=parse_area,
                        help="Selected area on patterns")
    parser.add_argument('--dset', default='yolo_class/select',
                        help="Dataset with result selection")
    parser.add_argument('-s', '--size', type=int, default=416, help="YOLO input size")
    parser.add_argument('-d', '--darknet',
                        default='/opt/yolov2/darknet',
                        help="Darknet binary")
    parser.add_argument('-p', '--spec',
                        default='/opt/yolov2/data/obj-single_hit.data',
                        help="Darknet data spec")
    parser.add_argument('-c', '--cfg',
                        default='/opt/yolov2/yolo-obj.cfg',
                        help="Darknet config file")
    parser.add_argument('-w', '--weights',
                        default='/opt/yolov2/yolo-obj_4000.weights',
                        help="YOLO weights")
    parser.add_argument('--keep', action='store_true', help="Keep converted images")
    parser.add_argument('-v', '--verbose', action='store_true', help="Verbose mode")
    parser_args = parser.parse_args()

    cxi_file = parser_args.cxi_file
    out_file = parser_args.out_file
    tmpdir_path = parser_args.tmp
    sel_area = parser_args.area
    class_dset = parser_args.dset
    yolo_size = parser_args.size
    darknet = parser_args.darknet
    yolo_spec = parser_args.spec
    yolo_cfg = parser_args.cfg
    yolo_weights = parser_args.weights
    keep_images = parser_args.keep
    verbose = parser_args.verbose

    for f in [cxi_file, yolo_spec, yolo_cfg, yolo_weights]:
        if not os.path.isfile(f):
            parser.error("File {} doesn't exist".format(f))

    if shutil.which(darknet) is None:
        parser.error("Cannot find executable {}".format(darknet))

    if out_file is not None and os.path.exists(out_file):
        parser.error("File {} already exists".format(out_file))

    if out_file is not None:
        copy_file_for_write(cxi_file, out_file)
    else:
        out_file = cxi_file

    if tmpdir_path is None:
        tmpdir = TemporaryDirectory()
        tmpdir_path = tmpdir.name
    else:
        tmpdir = None

    dn_tester = DarknetTester(darknet, yolo_spec, yolo_cfg, yolo_weights, verbose)
    dn_tester.start()

    test_cxi_file(out_file,
                  dn_tester,
                  class_dset,
                  tmpdir_path,
                  size=yolo_size,
                  sel_area=sel_area,
                  keep_images=keep_images)

    dn_tester.stop()

    if tmpdir is not None:
        tmpdir.cleanup()


if __name__ == '__main__':
    main()
