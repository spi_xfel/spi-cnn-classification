# Installation

SPI images classification by YOLO v2.
Scripts require **Python 3.6** or higher.

To install, clone the repository and use:
```
pip install ./spi-cnn-classification
```

# Usage

To classify CXI file with YOLO v2 please run:
```
spi_yolo_class.py [-h] [-o OUT_FILE] [-t TMP] [-a Y1,Y2,X1,X2]
                  [--dset DSET] [-s SIZE] [-d DARKNET] [-p SPEC]
                  [-c CFG] [-w WEIGHTS] [--keep] [-v]
                  FILE
```

Options:
* `-o OUT_FILE, --out_file OUT_FILE` - Output file
* `-t TMP, --tmp TMP` - Temporary directory
* `-a Y1,Y2,X1,X2, --area Y1,Y2,X1,X2` - Selected area on patterns
* `--dset DSET` - Dataset with result selection
* `-s SIZE, --size SIZE` - YOLO input size
* `-d DARKNET, --darknet DARKNET` - Darknet binary
* `-p SPEC, --spec SPEC` - Darknet data spec
* `-c CFG, --cfg CFG` - Darknet config file
* `-w WEIGHTS, --weights WEIGHTS` - YOLO weights
* `--keep` - Keep converted images
* `-v, --verbose` - Verbose mode