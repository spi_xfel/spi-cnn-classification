# YOLOv2
### (neural networks for objects detection)

Original repository (https://github.com/pjreddie/darknet)

### How to compile on Linux:
Run ./make


### How to use:

1. Place model (*.weights file) to the backup/ folder

2. For test run:
./darknet detector test data/obj-single_hit.data yolo-obj.cfg backup/*.weights {path to test image}

3. For training run:
./darknet detector train data/obj-single_hit.data yolo-obj.cfg backup/*.weights

data/obj-single_hit.data points to the file with the class name and to the list of training images
