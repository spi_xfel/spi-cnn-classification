from setuptools import setup

def readme():
    with open('README.md') as f:
        return f.read()

setup(name='spi_cnn_class',
      version='0.1.1',
      description='Module for YOLO based classification of diffraction images in CXI format.',
      long_description=readme(),
      long_description_content_type="text/markdown",
      url='https://gitlab.com/spi_xfel/spi-cnn-classification',
      author='Alexandr Ignatenko, Sergey Bobkov',
      author_email='alexandr.ignatenko@desy.de, s.bobkov@grid.kiae.ru',
      license='MIT',
      python_requires='>=3.6',
      install_requires=['matplotlib',
                        'pillow',
                        'h5py',
                        'tqdm'],
      scripts=['scripts/spi_yolo_class.py'],
      zip_safe=False)
