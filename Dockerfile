# Use the official image as a parent image
ARG cuda_version=10.2
ARG cuda_env=devel
ARG os=centos8

FROM nvidia/cuda:${cuda_version}-cudnn7-${cuda_env}-${os}

RUN yum update -y && \
    yum install -y make gcc gcc-c++ python3-pip

COPY yolov2 /opt/yolov2
COPY data/*weights /opt/yolov2/
ENV PATH=/opt/yolov2:$PATH

WORKDIR /opt/yolov2
RUN make

ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

COPY dist/spi_cnn_class*tar.gz .
RUN pip install spi_cnn_class*